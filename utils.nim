import os, strutils, algorithm, sequtils


type ImageRatio {.pure.} = enum
  is1by1  =  "is-1by1",
  is5by4  =  "is-5by4",
  is4by3  =  "is-4by3",
  is3by2  =  "is-3by2",
  is5by3  =  "is-5by3",
  is16by9 =  "is-16by9",
  is2by1  =  "is-2by1",
  is3by1  =  "is-3by1",
  is4by5  =  "is-4by5",
  is3by4  =  "is-3by4",
  is2by3  =  "is-2by3",
  is3by5  =  "is-3by5",
  is9by16 =  "is-9by16",
  is1by2  =  "is-1by2",
  is1by3  =  "is-1by3",
  default =  ""

type
  FolderOptions* = object
    imageRatio*: ImageRatio
    itemsPerRow*: int
  
  MediaItem* = object
   files*: seq[FileData]
   name*: string
   imagePath*: string
  
  FileData* = object
    name*: string
    path*: string
    size*: string

proc getFolderOptions*(folderPath: string = "."): FolderOptions =
  var folderOptions = FolderOptions()
  folderOptions.itemsPerRow = 3
  folderOptions.imageRatio = ImageRatio.default
  try:
    for line in lines(folderPath/"sambalshare.conf"):
      if line.startsWith("imageRatio"):
        folderOptions.imageRatio = parseEnum[ImageRatio](line.split("=")[^1].strip(),ImageRatio.default)
        continue
      if line.startsWith("itemsPerRow"):
        try:
          folderOptions.itemsPerRow = parseInt(line.split("=")[^1].strip())
        except:
          discard
  except:
    discard
  return folderOptions
        
proc newMediaItem*(): MediaItem =
  var mediaItem = MediaItem()
  mediaItem.files = newSeq[FileData]()
  return mediaItem

proc populateMediaItems*(): seq[MediaItem] =
  var items = newSeq[MediaItem]()
  for kind, path in ".".walkDir():
    if path.splitPath().tail.startsWith("."):
      continue
    if kind==pcDir:
      var mediaItem = newMediaItem()
      var (_, name) = path.splitPath
      mediaItem.name = name
      for filePath in walkDirRec(path):
        var fileData = FileData()
        fileData.path = filePath
        fileData.name = filePath.replace("."/mediaItem.name/"","")
        fileData.size = formatSize(filePath.getFileSize())
        mediaItem.files.add fileData
      mediaItem.files = mediaItem.files.sortedByIt(it.name)
      let imageCandidates = filterIt(mediaItem.files,
                            it.name.startsWith("_cover") and (
                            it.name.endsWith(".jpg") or
                            it.name.endsWith(".gif") or
                            it.name.endsWith(".png")))
      if imageCandidates.len > 0:
        mediaItem.imagePath = imageCandidates[0].path
      else:
        mediaItem.imagePath = "./pixel.png"
      
      items.add mediaItem

  return items.sortedByIt(it.name)