all: build-linux build-osx build-win
build-linux:
	choosenim devel
	nimble install rosencrantz -y
	nim c -d:musl -d:release -o:dist/linux_amd64/sambalshare sambalshare 

build-win:
	choosenim devel
	nimble install rosencrantz -y
	nim c --os:windows --cpu=amd64 -d:release -o:dist/windows_amd64/sambalshare.exe sambalshare

build-osx:
	choosenim devel
	nimble install rosencrantz -y
	nim c --os:macosx --cpu=amd64 -d:release -o:dist/darwin_amd64/sambalshare sambalshare
