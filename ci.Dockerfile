FROM alpine

ADD dist/linux_amd64/sambalshare /bin
RUN mkdir /data
VOLUME [ "/data" ]
EXPOSE 8080
CMD ["sh", "-c", "cd /data && sambalshare"]