import rosencrantz, asynchttpserver, strformat, asyncdispatch, os, uri

import utils
include "html.tmpl"

const fontawesome = slurp"./static/fa.all.js"
const bulma = slurp"./static/bulma.min.css"
const pixel = slurp"./static/pixel.png"

const nimVersion = gorge"nim -v"
const git = gorge"git rev-parse HEAD"

echo fmt"""
SambalShare
Revision: {git}
Built with:
{nimVersion}
"""


let sambalSharePort = getEnv("SAMBALSHARE_PORT","8080").string.parseInt
let sambalShareAddress = getEnv("SAMBALSHARE_ADDRESS","0.0.0.0").string

let handler = get[
  path("/")[
    contentType("text/html")[
      scope do:
        let items = populateMediaItems()
        let folderOptions = getFolderOptions()
        ok(generateHTMLPage(items, folderOptions))
]
  ] ~ # serve static files
  path("/fa.all.js")[
    contentType("application/javascript")[
      ok(fontawesome)
    ]
  ] ~
  path("/bulma.min.css")[
    contentType("text/css")[
      ok(bulma)
    ]
  ] ~
  path("/pixel.png")[
    contentType("image/png")[
      ok(pixel)
    ]
  ] ~ # serve current directory
  pathChunk("/")[
    pathEnd(proc(s: string): auto =
    if s.contains(".."):
      return notFound("404")
    if s.decodeUrl.existsFile:
      fileAsync(s.decodeUrl)
    else:
      notFound(fmt"Couldn't find {s}")
  )
  ]
]

let server = newAsyncHttpServer()
waitFor server.serve(Port(sambalSharePort), handler, address=sambalShareAddress)
