# SambalShare


## Description
SambalShare is a modern take on sharing your library of files to others. It is a web application written in Nim that compiles to a single binary executable for Windows, macOS, and Linux. Its goal is to be very lightweight and provide a simple user interface over your collection of files. 

## Screenshots
![Main View](https://gitlab.com/jivank/sambalshare/raw/master/screenshots/ss-main.png)
![Download View](https://gitlab.com/jivank/sambalshare/raw/master/screenshots/ss-download-detail.png)

## Roadmap
* Sharing between SambalShare instances
* Finding other nearby instances

## Setup
Simply download the artifacts from:

Place the binary in your path and change directory to the folder you want to server. Run `sambalshare`

To customize the port `SAMBALSHARE_PORT=9999 sambalshare`


## Folder Customization
Create a file named sambalshare.conf in the root folder.
```
itemsPerRow = 3
imageRatio = is-2by1
```

## Building
Requirements
* Git
* Nim
* Rosencrantz

Install Nim
Install rosencrantz
`nimble install rosencrantz`

Compile SambalShare
`nim c sambalshare`


## Docker
`docker run -p 8080:8080 -v <FOLDER_TO_SHARE>:/data registry.gitlab.com/jivank/sambalshare` 

## What does SambalShare mean?
Sambal is a spicy sauce from Indonesia. SambalShare is a play on SAMBA for Linux, which allows you to share a folder with Windows clients. 

## Why did you create SambalShare?
I am big fan of older games and occasionally will have a LAN party with friends. I wanted an "appstore" like experience for the items I curated in my library. 